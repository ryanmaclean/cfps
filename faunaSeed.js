require('dotenv').config({ path: '.envDBseed' })
const { Get, Client, CreateCollection, CreateIndex, CreateFunction } = require('faunadb');

const { indexData } = require('./fauna/indexes');
const { functionDetails } = require("./fauna/functions");

const client = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
})

console.log("💻 Starting DB seed");
seedDB();

async function seedDB() {
    console.log("Adding Collections");
    await addCollections();
    console.log("Creating Indexes");
    await createIndexes();
    console.log("Creating Functions");
    await createFunctions();
    console.log("✅ All done! Happy coding!")
}

async function addCollections() {
    client.query(CreateCollection({ name: 'cfps', history_days: null })).catch(e => console.error(e))
    client.query(CreateCollection({ name: 'talks', history_days: null })).catch(e => console.error(e))
    client.query(CreateCollection({ name: 'user_profiles', history_days: null })).catch(e => console.error(e))
    client.query(CreateCollection({ name: 'users', history_days: null })).catch(e => console.error(e))
}

async function createIndexes() {
    indexData.forEach(async (element) => {
        await client.query(CreateIndex({
            active: element.active,
            serialized: element.serialized,
            name: element.name,
            source: element.source,
            terms: element.terms,
            values: element.values,
            unique: element.unique
        })).catch(e => console.error(e))
    });
}

async function createFunctions() {
    functionDetails.forEach(async (element) => {
        await client.query(CreateFunction({
            name: element.name,
            body: element.body,
        }))
    })
}
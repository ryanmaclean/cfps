const getProfile = require('./get');
const createUserProfile = require('./create');
const updateProfile = require('./update')
// const deleteTalks = require('./delete')

const handler = async (event, context) => {
    const path = event.path.replace(/\.netlify\/functions\/[^/]+/, '')
    const segments = path.split('/').filter(Boolean)

    try {
        const { user, identity } = context.clientContext;
        const userId = user.sub;
        switch (event.httpMethod) {
            case 'GET':
                if (segments.length === 0) return getProfile.one(userId);
                if (segments.length === 1) {
                    const [id] = segments;
                    return getProfile.isInUse(id);
                }
                return {
                    statusCode: 400,
                    body: 'Too many segments in GET request.' //  Either / or /:id required'
                }
            case 'POST':
                return createUserProfile.setShortCode(event, userId);
            case 'PUT':
                if (segments.length === 1) {
                    const [id] = segments;
                    return updateProfile.one(userId, event);
                }
                return {
                    statusCode: 400,
                    body: 'Invalid segments.  Must be of the form /:id'
                }
            // case 'DELETE':
            //     if (segments.length === 1) {
            //         const [id] = segments;
            //         return deleteTalks.one(userId, id);
            //     }
            //     return {
            //         statusCode: 400,
            //         body: 'Invalid segments.  Must be of the form /:id'
            //     }
            default:
                console.log('Where is my large automobile?')
                return {
                    statusCode: 400,
                    body: 'Where is my large automobile?'
                }
        }
    } catch (error) {
        console.log('error', error)
        return {
            statusCode: 500,
            body: JSON.stringify(error),
        }
    }
}

module.exports = { handler }